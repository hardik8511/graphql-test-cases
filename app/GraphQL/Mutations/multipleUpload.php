<?php

namespace App\GraphQL\Mutations;

use App\Models\User;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Mutation;

class multipleUpload extends Mutation
{
    protected $attributes = [
        'name' => 'update the user image'
    ];

    public function type(): Type
    {
        return Type::nonNull(GraphQL::type('user'));
    }

    public function args(): array
    {
        return [
            'avatar' => [
                'type' => Type::getNullableType(GraphQL::type('Upload')),
                'description' => 'The photos of users',
            ]
        ];
    }

    public function resolve($root, array $args)
    {
        // $manuFactorData = ManufactorModel::create([
        //     'model_name' =>  $args['name'],
        //     'model_description' =>  $args['description'],
        //     'model_img' => "",
        // ]);
        // return $manuFactorData;
        dd($args);
        $path = $args['avatar']->storePublicly('pubilc/upload');
        // dd($path);
        $user_data =  User::find($args['id']);
        $user_data->avatar = $path;
        // dd($args);
        return $user_data;
    }
}
