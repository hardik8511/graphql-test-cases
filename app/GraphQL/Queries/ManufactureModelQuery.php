<?php

namespace App\GraphQL\Queries;

use App\Models\Manufactor;
use App\Models\ManufactorModel;
use App\Models\User;
use Closure;
use Rebing\GraphQL\Support\Facades\GraphQL;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;

class ManufactureModelQuery extends Query
{
    protected $attributes = [
        'name' => 'manufactor_model',
    ];

    public function type(): Type
    {
        return Type::nonNull(Type::listOf(Type::nonNull(GraphQL::type('manufacture_model'))));
    }

    public function args(): array
    {
        return [
            'id' => [
                'name' => 'id',
                'type' => Type::string(),
            ],
            'model_name' => [
                'name' => 'model_name',
                'type' => Type::string(),
            ],
            'model_description' => [
                'name' => 'model_description',
                'type' => Type::string(),
            ]
        ];
    }

    public function resolve($root, array $args)
    {
        if (isset($args['id'])) {
            return ManufactorModel::where('id', $args['id'])->get();
        }

        if (isset($args['model_name'])) {
            return ManufactorModel::where('model_name', $args['model_name'])->get();
        }

        return ManufactorModel::all();
    }
}
