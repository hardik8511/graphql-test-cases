<?php
namespace App\GraphQL\Queries\Auth;

use App\Models\User;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Password;
use Exception;

class LoginQuery extends Query
{
    protected $attributes = [
        'name' => 'login',
        'description' => 'User login'
    ];

    public function type(): Type
    {
        return GraphQL::type('Auth');
    }

    public function args(): array
    {
        return [
            'email' => [
                'name' => 'email',
                'type' => Type::string(),
                'rules' => ['required', 'email']
            ],
            'password' => [
                'name' => 'password',
                'type' => Type::string(),
                'rules' => ['required']
            ]
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $info, Closure $getSelectFields)
    {

        $user = User::where(['email' => $args['email']])->firstOrFail();
        // dd($user);
        $auth_token = '';
        if ($user) {
            if (Hash::check($args['password'], $user->password)) {
                // dd("login success");
                $user->tokens()->delete();
                Auth::login($user);
                $auth_token = $user->createToken("user_token")->plainTextToken;
                // dd($auth_token);
            }
            else{
                throw new Exception("Invalid email or password");
                return null;
            }
        }
        // dd($auth_token);
        return ['token' => $auth_token];
    }
}
?>