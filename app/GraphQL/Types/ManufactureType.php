<?php

namespace App\GraphQL\Types;

use App\Models\Manufactor;
use App\Models\User;
use Rebing\GraphQL\Support\Facades\GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class ManufactureType extends GraphQLType
{
    protected $attributes = [
        'name'          => 'manufacture',
        'description'   => 'A Manufacture Model',
        'model'         => Manufactor::class,
    ];

    public function fields(): array
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The id of the manufacture',
            ],
            'name' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The name of manufacture',
            ],
            'description' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The description of manufacture',

            ],
            // reletionship for manufacture has many models
            'manufacture_models' => [
                'type' => Type::listOf(GraphQL::type('manufacture_model')),
                'description' => 'manufactures has many models',
            ],
        ];
    }

    // You can also resolve a field by declaring a method in the class
    // with the following format resolve[FIELD_NAME]Field()
    // protected function resolveEmailField($root, array $args)
    // {
    //     return strtolower($root->email);
    // }
}
