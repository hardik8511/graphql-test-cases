<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('manufactor_models', function (Blueprint $table) {
            $table->id();
            // $table->index('manufacture_id');
            $table->unsignedBigInteger('manufacture_id')->index();
            $table->foreign('manufacture_id')->references('id')->on('manufactors')->onDelete('cascade');
            $table->string('model_name');
            $table->string('model_description');
            $table->string('model_img');
            $table->enum('is_active',['y','n'])->default('y')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('manufactor_models');
    }
};
