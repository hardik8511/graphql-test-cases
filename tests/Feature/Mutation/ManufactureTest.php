<?php

namespace Tests\Feature\Mutation;

use App\Models\Manufactor;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ManufactureTest extends TestCase
{
    use WithFaker;
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    // public function test_example()
    // {
    //     $response = $this->get('/');

    //     $response->assertStatus(200);
    // }


    public function test_validation_fail_create_manufacture()
    {
        $query = 'mutation{
            createManufacture(name:"", description:"Test Hardik Description"){
                id
                name
            }
        }';

        $expected = [
            'errors' => [
                [
                    'message',
                    //here name is required for the crating the validation
                ]
            ]
        ];

        return $this->returnResult($query, $expected);
    }

    public function test_create_manufacture_sucessfully()
    {
        $query = 'mutation($name:String!,$description:String!){
            createManufacture(name:$name, description:$description){
                id
                name
                description
            }
        }';

        $expected = [
            'data' => [
                "createManufacture" =>
                [
                    'name',
                    'description',
                ]
            ]
        ];

        $variables = [
            'name' => $this->faker->name(),
            'description' => $this->faker->text(10)
        ];

        return $this->returnResult($query, $expected, $variables);
    }


    public function test_update_manufacture_but_records_is_not_found()
    {
        $query = 'mutation{
            editManuFacture(id:9, name:"Test Update Name", description:"Test update description" ){
                id
                name
                description
            }
        }';

        $expected = [
            'errors' => [
                [
                    'message',
                ]
            ]
        ];

        $variables = [
            'name' => $this->faker->name(),
            'description' => $this->faker->text(10)
        ];

        return $this->returnResult($query, $expected, $variables);
    }

    public function test_upate_manufacure_sucessfully()
    {
        // $fack_data = Manufactor::factory()->create();
        Manufactor::create([
            "name" => $this->faker->name(),
            "description" => $this->faker->name(),
        ]);

        $query = 'mutation{
            editManuFacture(id:1, name:"Test Update Name", description:"Test update description" ){
                id
                name
                description
            }
        }';

        $expected = [
            "data" => [
                "editManuFacture" => [
                    "id",
                    "name",
                    "description",
                ]
            ]
        ];

        $variables = [
            'name' => $this->faker->name(),
            'description' => $this->faker->text(10)
        ];

        return $this->returnResult($query, $expected, $variables);
    }


    public function test_delete_manufacture_but_record_not_found()
    {
        $query = 'mutation{
            deleteManuFacture(id:1){
                id
                name
            }
         }';

        $expected = [
            'errors' => [
                [
                    'message',
                ]
            ]
        ];

        $this->returnResult($query, $expected);
    }

    public function test_delete_manufacture_sucessfully()
    {
        $query = 'mutation($id:Int!){
            deleteManuFacture(id:$id){
                id
                name
            }
         }';

        $expected = [
            "data" => [
                "deleteManuFacture" => [
                    "id",
                    "name",
                ]
            ]
        ];

        $variables = [
            'id' => 4,
        ];

        $this->returnResult($query, $expected, $variables);
    }
}
